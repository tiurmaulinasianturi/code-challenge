const {
  barang,
  pelanggan,
  transaksi
} = require('../../models')
const { check, validationResult, matchedData, sanitize } = require('express-validator'); // Import express-validator

module.exports = {
    create: [
      check('id_barang').custom(value => {
        return client.db('penjualan_dev').collection('barang').findOne({
          _id: new ObjectId(value)
        }).then(result => {
          if (!result) {
            throw new Error('ID Barang tidak ada')
          }
        })
      }),
      check('id_pelanggan').custom((value) => {
        return client.db('penjualan_dev').collection('pelanggan').findOne({
          _id: new ObjectId(value)
        }).then(result => {
          if (!result) {
            throw new Error('ID Pelanggan tidak ada')
          }
        })
      }),
      check('jumlah').isNumeric(),
      (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next()
      }
    ],
    update: [
        check('id').custom(value => {
          return client.db('penjualan_dev').collection('transaksi').findOne({
            _id: new ObjectId(value)
          }).then(result => {
            if (!result) {
              throw new Error('ID Transaksi tidak ada')
            }
          })
        }),
        check('id_barang').custom(value => {
          return client.db('penjualan_dev').collection('barang').findOne({
            _id: new ObjectId(value)
          }).then(result => {
            if (!result) {
              throw new Error('ID Barang tidak ada')
            }
          })
        }),
        check('id_pelanggan').custom(value => {
          return client.db('penjualan_dev').collection('pelanggan').findOne({
            _id: new ObjectId(value)
          }).then(result => {
            if (!result) {
              throw new Error('ID Pelanggan tidak ada')
            }
          })
        }),
        check('jumlah').isNumeric(),
        (req, res, next) => {
          const errors = validationResult(req)
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next()
        }
      ],
    delete :[
      check('id').custom((value)=> {
        return client
        .db('penjualan')
        .collection('transaksi')
        .findOne({
          _id: new ObjectID(value),
        })
        .then ((result)=>{
          if (!result) {
            throw new Error("ID transaksi tidak ada!");
          }
        });
      }),
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return Response.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      },
    ],
  }