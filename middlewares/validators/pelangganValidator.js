const { check, validationResult, matchedData, sanitize } = require('express-validator'); // Import express-validator
const client = require('../../models/connection.js') // Import connection
const { ObjectId } = require('mongodb') // Import ObjectId

module.exports = {
    create: [
      check('nama').custom(value => {
        return client.db('penjualan_dev').collection('pelanggan').findOne({
          nama : value,
        }).then(result => {
          if (!result) {
            throw new Error('Nama Pelanggan Sudah Ada')
          }
        });
      }),
      (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      },
    ],
    update: [
        check('id').custom((value) => {
          return client.db('penjualan_dev').collection('pelanggan').findOne({
            _id: new ObjectId(value),
          }).then((result) => {
            if (!result) {
              throw new Error('ID Pelanggan tidak ada')
            }
          })
        }),
        check('nama').isLength( {min: 3}),
        (req, res, next) => {
          const errors = validationResult(req)
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        },
      ],
      delete: [
          check("id").custom((value) => {
              return client
              .db('penjualan')
              .collection("pelanggan")
              .findOne({
                  _id: new ObjectID(value),
              })
              .then ((result)=>{
                  if (!result) {
                      throw new Error("ID pelanggan tidak ada");

                  }
              });
            }),
            (req, res, next) => {
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(422).json({
                        errors: errors.mapped()

             });
            }
            next();
          },
      ],
}
    