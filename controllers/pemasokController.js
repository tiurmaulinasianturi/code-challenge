const client= require("../models/connection")
const {ObjectId} = require("mongodb")
const { check, validationResult, matchedData, sanitize } = require('express-validator'); //form validation & sanitize form params
const penjualan = client.db('penjualan_dev') // Connect to penjualan database
const pemasok = penjualan.collection('pemasok') // Connect to transaksi collection / table

class PemasokController {


// get All data
async getAll(req, res) {
    
    // find all transaksi data
    pemasok.find({}).toArray().then(result => {
      res.json({
        status: "success",
        data: result
      })
    })
  }
// get one data
async getOne(req, res) {
    

    // Find one data
    pemasok.findOne({
      _id: new ObjectId(req.params.id)
    }).then(result => {
      res.json({
        status: "success",
        data: result
      })
    })
  }
// create a data
async create(req, res) {
   


    // Insert data transaksi
    pemasok.insertOne({
     nama : req.body.nama
    }).then(result => {
      res.json({
        status: "success",
        data: result.ops[0]
      })
    })
  }
// update a data
async update(req, res) {
  
    
    // Update data depends on transaksi
    pemasok.updateOne({
      _id: new ObjectId(req.params.id)
    }, {
      $set: {
        nama :req.body.nama
      }
    }).then(() => {
      return pemasok.findOne({
        _id: new ObjectId(req.params.id)
      })
    }).then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }
// delete data
async delete(req, res) {
    

    // delete data depends on req.params.id
    pemasok.deleteOne({
      _id: new ObjectId(req.params.id)
    }).then(result => {
      res.json({
        status: 'success',
        data: null
      })
    })
  }

}
module.exports = new PemasokController;
