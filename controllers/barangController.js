const client = require("../models/connection");
const { ObjectId } = require("mongodb");
const { check, validationResult, matchedData, sanitize } = require('express-validator'); //form validation & sanitize form params

class BarangController {

  // get All data
  async getAll(req, res) {
    const penjualan = client.db("penjualan_dev"); // Connect to penjualan database
    const barang = penjualan.collection("barang"); // Connect to transaksi collection / table

    // find all transaksi data
    barang
      .find({})
      .toArray()
      .then((result) => {
        res.json({
          status: "success",
          data: result,
        });
      });
  }
  // get one data
  async getOne(req, res) {
    const penjualan = client.db("penjualan_dev"); // Connect to penjualan database
    const barang = penjualan.collection("barang"); // Connect to transaksi collection / table

    // Find one data
    barang
      .findOne({
        _id: new ObjectId(req.params.id),
      })
      .then((result) => {
        res.json({
          status: "success",
          data: result,
        });
      });
  }
  // create a data
  async create(req, res) {
    const penjualan = client.db("penjualan_dev"); // Connect to penjualan database
    const barang = penjualan.collection("barang"); 
    const pemasok = penjualan.collection('pemasok')

    // Get data barang depends on req.body.id_barang
    const getPemasok = await penjualan.collection("barang").findOne({
      _id: new ObjectId(req.body.id_pemasok),
    });

   
    barang
      .insertOne({
        nama: req.body.nama,
        harga: req.body.harga,
        pemasok: pemasok
      })
      .then((result) => {
        res.json({
          status: "success",
          data: result.ops[0],
        });
      });
  }
  // update a data
  async update(req, res) {
    const penjualan = client.db("penjualan_dev"); // Connect to penjualan database
    const pemasok = penjualan.collection("pemasok");
    const barang = penjualan.collection('barang')

    // Get data barang depends on req.body.id_pemasuk
    const Getpemasok= await penjualan.collection("pemasok").findOne({
      _id: new ObjectId(req.body.id_pemasok),
    });
    barang
      .updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: {
            nama: req.body.nama,
            harga: req.body.harga,
            pemasok :pemasok
          },
        }
      )
      .then(() => {
        return transaksi.findOne({
          _id: new ObjectId(req.params.id),
        });
      })
      .then((result) => {
        res.json({
          status: "success",
          data: result,
        });
      });
  }
  // delete data
  async delete(req, res) {
    const penjualan = client.db("penjualan_dev"); // Connect to penjualan database
    const barang= penjualan.collection("barang"); // Connect to transaksi collection / table

    // delete data depends on req.params.id
    barang
      .deleteOne({
        _id: new ObjectId(req.params.id),
      })
      .then((result) => {
        res.json({
          status: "success",
          data: null,
        });
      });
  }
}
module.exports = new BarangController();
