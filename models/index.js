const mongoose = require('mongoose');
const uri = "mongodb://localhost:27017/penjualan_dev"
mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true})
const barang = require('./barang.js')
const pemasok = require('./pemasok.js')
const pelanggan = require('./pelanggan.js')
const transaksi = require('./transaksi.js')

module.exports = {barang, pelanggan, pemasok, transaksi}